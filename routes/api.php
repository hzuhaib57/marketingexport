<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\SubcategoryController;
use App\Http\Controllers\Api\LevelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'v1'], function ($router) {
    Route::middleware('auth:sanctum')->group(function(){
        Route::get('/user',[UserController::class, 'getUser']);
        //----------------------- category routes start ----------------------//

        Route::get('get/categories',[CategoryController::class,'getCategories'])->name('category');// categories list

        //----------------------- category routes end ----------------------//

        //----------------------- sub category routes start ----------------------//

        Route::get('get/subcategories',[SubcategoryController::class,'getSubCategories'])->name('subcategory');// sub categories list

        //----------------------- sub category routes end ----------------------//
    });
    Route::get('get/levels',[LevelController::class,'getAllLevels']);
    Route::post("login",[UserController::class, 'Login']);
    Route::post("register",[UserController::class, 'Register']);
});



