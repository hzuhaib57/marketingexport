<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'home']);

Route::middleware('auth')->group(function(){
    
    Route::get('/AdminDashboard', 'App\Http\Controllers\HomeController@index')->name('home');
    Route::get('/UserDashboard', 'App\Http\Controllers\HomeController@UserDashboard')->name('userdashboard');
    Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	Route::get('map', function () {return view('pages.maps');})->name('map');
	Route::get('icons', function () {return view('pages.icons');})->name('icons'); 
	Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
    
	
    // Routes for reading files

    Route::get('/file-upload', 'App\Http\Controllers\DashboardController@fileupload')->name('fileupload')->middleware('auth');
	Route::Post('/file-push', 'App\Http\Controllers\FilesController@filePush')->name('filepush')->middleware('auth');
	Route::Post('/file-msa-push', 'App\Http\Controllers\FilesController@fileMsaPush')->name('filemsapush')->middleware('auth');
	Route::Post('/file-household-push', 'App\Http\Controllers\FilesController@filehouseholdpush')->name('filehouseholdpush')->middleware('auth');

	Route::Post('/historical-content-add', 'App\Http\Controllers\FilesController@historicalContentAdd')->name('historicalContentAdd')->middleware('auth');

	
	// Route::Post('/file-fairrent-push', 'App\Http\Controllers\FilesController@filefairrentpush')->name('filefairrentpush')->middleware('auth');
	
    // Routes for reading files
    
});

Auth::routes(['register'=>false]);
Auth::routes();

