@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-md-12">
            <h2>Upload Historical Rental Vacancy Rate data File</h2>
            <div class="card">
              <div class="card-body">
                <form id="fileupload" enctype="multipart/form-data" action="{{ route('filepush') }}" method="post">
                    @csrf
                    <input required accept=".xlsx" type="file" name="fileupload">
                    <p></p>
                    <textarea name="historicalContent" id="historicalContent"></textarea>
                    <p></p>
                    <input type="submit" class="btn btn-primary">
                </form>
              </div>
            </div>
          </div>
        </div>
    
        <div class="row mt-3">
          <div class="col-md-12">
            <h2>Zip Code Wise Rent</h2>
            <div class="card">
              <div class="card-body">
                <form id="filemsaupload" enctype="multipart/form-data" action="{{ route('filemsapush') }}" method="post">
                    @csrf
                    <input required accept=".xlsx" type="file" name="filemsaupload">
                    <p></p>
                    <textarea name="zipcodeContent" id="zipcodeContent"></textarea>
                    <p></p>
                    <input type="submit" class="btn btn-primary">

                </form>
              </div>
            </div>
           
          </div>
        </div>
    
        <div class="row mt-3">
          <div class="col-md-12">
            <h2>Upload Average Rent By Neighborhood Ja File</h2>
            <div class="card">
              <div class="card-body">
                <form id="filemsaupload" enctype="multipart/form-data" action="{{ route('filehouseholdpush') }}" method="post">
                    @csrf 
                    <input required accept=".xlsx" type="file" name="filehouseholdupload">
                    <p></p>
                    <textarea name="neighborhoodContent" id="neighborhoodContent"></textarea>
                    <p></p>
                    <input type="submit" class="btn btn-primary">
                </form>
              </div>
            </div>
          </div>
          
        </div>


        
    
    
      </div>
    </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
        selector: 'textarea#zipcodeContent', // Replace this CSS selector to match the placeholder element for TinyMCE
        plugins: 'code table lists',
        toolbar: 'undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
      });

      tinymce.init({
        selector: 'textarea#historicalContent', // Replace this CSS selector to match the placeholder element for TinyMCE
        plugins: 'code table lists',
        toolbar: 'undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
      });

      tinymce.init({
        selector: 'textarea#neighborhoodContent', // Replace this CSS selector to match the placeholder element for TinyMCE
        plugins: 'code table lists',
        toolbar: 'undo redo | formatselect| bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
      });
      
    </script>
@endpush