@extends('layouts.master')
@section('title','Dashboard')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit User</h1>
    <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/user">User Management</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit User</li>
    </ol> -->
    <a href="/user/{{$userType}}" class="btn btn-primary">Back <i class="fa fa-arrow-left"></i></a>
</div>
<form method="POST" action="/user/update" enctype='multipart/form-data'>
    @csrf
    @method('PUT')
    <input type="hidden" name="id" value="{{$user->id}}">
    <div class="form-group">
        <label for="fname">First Name *</label>
        <input type="text" name="fname" class="form-control" id="fname" aria-describedby="fname" placeholder="Enter First Name" value="{{$user->fname}}" required>
    </div>
    <div class="form-group">
        <label for="lname">Last Name *</label>
        <input type="text" name="lname" class="form-control" id="lname" aria-describedby="lname" placeholder="Enter Last Name" value="{{$user->lname}}" required>
    </div>
    <div class="form-group">
        <label for="email">Email *</label>
        <input type="email" name="email" class="form-control" id="email" aria-describedby="email" placeholder="Enter Email" value="{{$user->email}}" required>
    </div>
    <div class="form-group">
        <label for="phone">Phone Number *</label> 
        <input type="tel" class="form-control" id="phone" name="phone" value="{{$user->phone}}" required>
    </div>
    <div class="form-group">
        <label for="image">Description</label>
        <textarea class="form-control" name="description" id="" rows="3">{{$user->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="image">Profile Image</label>
        <input type="file" class="form-control" id="image" name="image">
    </div>
    <input type="hidden" name="role" value="{{$user->roles[0]->id}}">
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection