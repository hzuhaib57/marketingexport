@extends('layouts.master')
@section('title','Dashboard')
@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div class="d-flex">
        <h1 class="h3 mb-0 text-gray-800">{{$userType}}</h1>
        <div class="add-user ml-3">
            <a class="btn btn-success" href="/user/{{$userType}}/create"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">User Management</li>
    </ol> -->
</div>
    
<table class="table table-bordered" id="user">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Profile Pic</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $item)
        <tr>
            <td>{{$loop->index + 1}}</td>
            <td>{{$item->fname}}</td>
            <td>{{$item->lname}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->phone}}</td>
            <td>
                @if($item->img != "")
                <a href="javascript:;" id="showImg" data-img="{{asset('user_imgs')}}/{{$item->img}}" class="showImg">
                    <img src="{{asset('user_imgs')}}/{{$item->img}}" alt="" width="50">
                </a>
                @endif
            </td>
            <td>
                <a href="/user/{{$userType}}/edit/{{$item->id}}" class="btn btn-primary mx-3"><i class="fa fa-edit"></i></a>
                <a href="javascript:;" data-href="/user/{{$userType}}/delete/{{$item->id}}" class="btn btn-danger delBtn"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="modal" id="imgModal">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <img src="" alt="" width="100%">
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
@endsection