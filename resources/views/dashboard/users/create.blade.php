@extends('layouts.master')
@section('title','Dashboard')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{$type}}</h1>
    <!-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/user">User Management</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add User</li>
    </ol> -->
    <a href="/user/{{$userType}}" class="btn btn-primary">Back <i class="fa fa-arrow-left"></i></a>
</div>
<form method="POST" action="/user/add" enctype='multipart/form-data'>
    @csrf    
    <div class="form-group">
        <label for="fname">First Name *</label>
        <input type="text" name="fname" class="form-control" id="fname" aria-describedby="fname" placeholder="Enter First Name" required>
    </div>
    <div class="form-group">
        <label for="lname">Last Name *</label>
        <input type="text" name="lname" class="form-control" id="lname" aria-describedby="lname" placeholder="Enter Last Name" required>
    </div>
    <div class="form-group">
        <label for="email">Email *</label>
        <input type="email" name="email" class="form-control" id="email" aria-describedby="email" placeholder="Enter Email" required>
    </div>
    <div class="form-group">
        <label for="password">Password *</label>
        <input type="password" name="password" class="form-control" id="password" aria-describedby="password" placeholder="Enter Password" required>
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirm Password *</label>
        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" aria-describedby="password_confirmation" placeholder="Password Confirmation" required>
    </div>
    <div class="form-group">
        <label for="phone">Phone Number *</label> 
        <input type="tel" class="form-control" id="phone" name="phone" required>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" id="description" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="image">Profile Image</label>
        <input type="file" class="form-control" id="image" name="image">
    </div>
    <input type="hidden" name="role" value="{{$role->id}}">
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection