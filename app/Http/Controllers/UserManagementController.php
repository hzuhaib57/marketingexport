<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;
use App\Models\Level;

class UserManagementController extends Controller
{
    //
    public function index($type){
        try {
            $rol = 'customer';
            if($type == 'trainers'){
                $rol = 'admin';
            }
            $users = User::whereHas('roles', function($role)use($rol) {
                $role->where('name', '=', $rol);
            })->with(['roles' => function($role)use($rol) {
                $role->where('name', '=', $rol);
            }])->get();
            $userType = ucfirst($type);
            return view('dashboard.users.index',compact('users','userType'));
        } catch (Exception $e) {
            //throw $th;
        }
    }

    public function create($type){
        try {
            $rol = 'customer';
            if($type == 'Trainers'){
                $rol = 'admin';
            }
            $role = Role::where('name',$rol)->first();
            $userType = lcfirst($type);
            return view('dashboard.users.create',compact('role','userType','type'));
        } catch (Exception $e) {
            //throw $th;
        }
        
    }

    public function store(Request $request){
        try {
            $request->validate([
                'fname'                 => 'required',
                'lname'                 => 'required',
                'email'                 => 'required',
                'password'              => 'required|confirmed',
                'password_confirmation' => 'required',
                'phone'                 => 'required',
                'role'                  => 'required'
            ]);
            $filename = "";
            if($request->hasFile('image')){
                $file      = $request->file('image');
                $filename  = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                if(!file_exists(public_path('user_imgs/'.$filename))){
                    $file->move(public_path('user_imgs'), $filename);
                }
            }
            $role              = Role::where('id',$request->role)->first();
            $user              = new User;
            $user->fname       = $request->fname;
            $user->lname       = $request->lname;
            $user->email       = $request->email;
            $user->password    = Hash::make($request->password);
            $user->phone       = $request->phone;
            $user->description = $request->description;
            $user->img         = $filename;
            $user->save();
            $user->roles()->attach($role);
            $type = 'user/trainers';
            if($role->name == 'customer'){
                $type = 'user/customers';
            }
            return redirect($type)->with('success','User added successfully.');
        } catch (Exception $e) {
            //throw $th;
        }
        
    }

    public function edit($type,$id){
        try {
            $userType = lcfirst($type);
            $user = User::with('roles')->find($id); 
            return view('dashboard.users.edit',compact('user','userType'));
        } catch (Exception $e) {
            //throw $th;
        }
    }

    public function update(Request $request){
        try {

            $request->validate([
                'fname'         => 'required',
                'lname'         => 'required',
                'email'         => 'required',
                'phone'         => 'required',
                'role'          => 'required' 
            ]);
            $filename = "";
            if($request->hasFile('image')){
                $file      = $request->file('image');
                $filename  = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                if(!file_exists(public_path('user_imgs/'.$filename))){
                    $file->move(public_path('user_imgs'), $filename);
                }
            }
            $role              = Role::where('id',$request->role)->first();
            $user              = User::find($request->id);
            $user->fname       = $request->fname;
            $user->lname       = $request->lname;
            $user->email       = $request->email;
            $user->phone       = $request->phone;
            $user->description = $request->description;
            if($filename != ""){
                $user->img = $filename;
            }
            $user->save();
            $user->roles()->detach();
            $user->roles()->attach($role);
            $type = 'user/trainers';
            if($role->name == 'customer'){
                $type = 'user/customers';
            }
            return redirect($type)->with('success','User updated successfully');
        } catch (Exception $e) {
            //throw $th;
        }
               
    }

    public function destroy($type,$id){
        try {
            $user = User::find($id);
            $user->roles()->detach();
            $user->delete();
            $userType = lcfirst($type);
            $redirect = 'user/trainers';
            if($userType == 'customers'){
                $redirect = 'user/customers';
            }
            return redirect($redirect)->with('success','User deleted successfully');
        } catch (Exception $e) {
            //throw $th;
        }
        
    }
}
