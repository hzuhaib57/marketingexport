<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleXLSX;
use App\Models\ResidentPopulation;
use App\Models\MsaPopulationChange;
use App\Models\HistoricalRentRate;
use App\Models\ZipcodeRent;
use App\Models\HouseholdIncome;
use App\Models\historicalcontent;
use App\Models\ZipcodeContent;
use App\Models\AverageRentContent;
use Illuminate\Support\Str;
use App\Models\AverageRentByJa;

use Aspera\Spreadsheet\XLSX\Reader;
use Illuminate\Support\Facades\Redirect;

class FilesController extends Controller
{

    public function filepush(Request $request){
        $data = $request->all();    
        $content = $request->historicalContent;
        $fileId = Str::random(10);
        $contentData = new historicalcontent;
        $contentData->content = $content;
        $contentData->file_id = $fileId;
        $contentData->save();
        $file = $request->file('fileupload');    
        $xlsx = SimpleXLSX::parse($file);   
        if ( $xlsx = SimpleXLSX::parse($request->fileupload)) {
            $header_values = $rows = [];
            foreach ( $xlsx->rows() as $k => $r ) {
                if ( $k === 0 ) {
                    $header_values = $r;
                    continue;
                }
                $rows[] = array_combine( $header_values, $r );
            }
        }
        $lenght = count($rows);
        for ($i = 0; $i < $lenght; $i++){
            $year = $rows[$i]['Year'];
            $jacksonville = $rows[$i]["Jacksonville"];
            $florida = $rows[$i]["Florida"];
            $us = $rows[$i]["US"];
            $Values[] =  $year.','.$jacksonville.','.$florida.','.$us;
        }
        if(!empty($Values)){
            foreach($Values as $data){
                $dataValueArr = explode(',', $data);
                $store = new HistoricalRentRate; 
                $store->file_id = $fileId;
                $store->year = $dataValueArr[0];
                $store->jacksonville = $dataValueArr[1];
                $store->florida = $dataValueArr[2];
                $store->us = $dataValueArr[3];
                $store->save();                
            }
            return Redirect::back()->with('message', 'Data Added Successfully');    
        }



    }


    public function fileMsaPush(Request $request){
  
        $data = $request->all();
        $content = $request->zipcodeContent;
        $fileId = Str::random(10);
        $contentData = new ZipcodeContent;
        $contentData->file_id = $fileId;
        $contentData->content = $content;
        $contentData->save();
        
        if ( $xlsx = SimpleXLSX::parse($request->filemsaupload)) {
            $header_values = $rows = [];
            foreach ( $xlsx->rows() as $k => $r ) {
                if ( $k === 0 ) {
                    $header_values = $r;
                    continue;
                }
                $rows[] = array_combine( $header_values, $r );
            }
        }

        $lenght = count($rows);
        for ($i = 0; $i < $lenght; $i++){
            $zipCode = $rows[$i]['Zip Code'];
            $Rent = $rows[$i]["Rent"];

            $Values[] =  $zipCode.','.$Rent;
        }

        if(!empty($Values)){
            foreach($Values as $data){
                $dataValueArr = explode(',', $data);

                $store = new ZipcodeRent;
                $store->file_id = $fileId;
                $store->zip_code = $dataValueArr[0];
                $store->rent = $dataValueArr[1];
                $store->save();                
                
            }
            return Redirect::back()->with('message', 'Data Added Successfully');    
        }
    }


    public function filehouseholdpush(Request $request){


        $data = $request->all();
        $content = $request->neighborhoodContent;
        $fileId = Str::random(10);
        $contentData = new AverageRentContent;
        $contentData->file_id = $fileId;
        $contentData->content = $content;
        $contentData->save();

        if ( $xlsx = SimpleXLSX::parse($request->filehouseholdupload)) {
            $header_values = $rows = [];
            foreach ( $xlsx->rows() as $k => $r ) {
                if ( $k === 0 ) {
                    $header_values = $r;
                    continue;
                }
                $rows[] = array_combine( $header_values, $r );
            }
        }
        
        $lenght = count($rows);
        for ($i = 0; $i < $lenght; $i++){
            $City = $rows[$i]['City'];
            $Rent = $rows[$i]["Rent"];
            $Values[] =  $City.','.$Rent;
        }

        if(!empty($Values)){
            foreach($Values as $data){
                $dataValueArr = explode(',', $data);

                $store = new AverageRentByJa;
                $store->city = $dataValueArr[0];
                $store->file_id = $fileId;
                $store->rent = $dataValueArr[1];                
                $store->save();

            }
            return Redirect::back()->with('message', 'Data Added Successfully');    
        }

    }

    public function filefairrentpush(Request $request){

        if ( $xlsx = SimpleXLSX::parse($request->filehouseholdupload)) {
            $header_values = $rows = [];
            foreach ( $xlsx->rows() as $k => $r ) {
                if ( $k === 0 ) {
                    $header_values = $r;
                    continue;
                }
                $rows[] = array_combine( $header_values, $r );
            }
        }

        $lenght = count($rows);

        for ($i = 0; $i < $lenght; $i++){
            $date = $rows[$i]['Date'];
            $us = $rows[$i]["US"];
            $florida = $rows[$i]["Florida"];
            $orlando = $rows[$i]["Orlando"];
            $Values[] =  $date.','.$us.','.$florida.','.$orlando;
        }

        if(!empty($Values)){
            foreach($Values as $data){
                $dataValueArr = explode(',', $data);
                $store = new HouseholdIncome;
                $store->date = $dataValueArr[0];
                $store->us = $dataValueArr[1];
                $store->florida = $dataValueArr[2];
                $store->orlando = $dataValueArr[3];
                
                $store->save();

            }
            return Redirect::back()->with('message', 'Data Added Successfully');    
        }


    }

    public function historicalContentAdd(Request $request){
        $data = $request->all();
        dd($data);
    }

}
