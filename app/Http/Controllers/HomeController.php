<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use App\Models\HistoricalRentRate;
use App\Models\ZipcodeRent;
use App\Models\AverageRentByJa;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $datas = HistoricalRentRate::all();
            
        
        foreach($datas as $data){

            $val = (float)$data->jacksonville;
            $jacksonvila[] =  $val;
        }
        $jacksonvila = json_encode($jacksonvila);   

        $rents = ZipcodeRent::all();
        foreach($rents as $rent){
            $rentval = (float)$rent->rent;
            $rentsval[] =  $rentval;
        }

        $rentsval = json_encode($rentsval);  
        
        $rentbyjas = AverageRentByJa::all();
        foreach($rentbyjas as $rentbyja){
            $javal = (float)$rentbyja->rent;
            $jalArr[] =  $javal;
            $val = (float)$rentbyja->city;
            $labels[] = $val;
            
        }

        $jalArr = json_encode($jalArr);
        $labelArr = json_encode($labels);
        
        
        
        
             
        return view('dashboard',compact('jacksonvila','rentsval','jalArr','labelArr'));
    }

    public function userdashboard()
    {
        return view('userdashboard');
    }

    public function home()
    {
       return Redirect::to('/login');
    }
    

}
