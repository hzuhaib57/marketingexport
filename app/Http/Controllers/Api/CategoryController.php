<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    //

  public function getCategories(){
    try {
        $category = Category::get();
        return response()->json(['status'=>'ok','categories'=>$category]);
    } catch (Exception $e) {
      return response()->json(['status'=>'error','message'=>$e]);
    }
  }
}
