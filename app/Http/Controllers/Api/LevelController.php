<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Level;

class LevelController extends Controller
{
    public function getAllLevels(){
        try {
            $levels = Level::get();
            return response()->json(["status"=>"ok","levels"=>$levels]);
        } catch (Exception $e) {
            //throw $th;
        }
    }
}
