<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Subcategory;

class SubcategoryController extends Controller
{
    //
    public function getSubCategories(){
        try {
            $subCategory = Subcategory::with(['categories'])->get();
            return response()->json(['status'=>'ok','subCategories'=>$subCategory]);
        } catch (Exception $e) {
            return response()->json(['status'=>'error','message'=>$e]);
        }
    }
}
