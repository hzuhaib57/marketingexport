<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function Login(Request $request){
        try {
            $user = User::where('email',$request->email)->first();

            if(!$user || !HASH::check($request->password,$user->password)){
                return response()->json(['error'=>'Invalid credentials']);
            }

            return response()->json(['token'=>$user->createToken('auth')->plainTextToken]);
        } catch (Exception $e) {
            return response()->json(["status"=>"error","message"=>$e]);
        }
        
    }

    public function register(Request $request){
        try {
            $filename = "";
            if($request->hasFile('image')){
                $file      = $request->file('image');
                $filename  = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                if(!file_exists(public_path('user_imgs/'.$filename))){
                    $file->move(public_path('user_imgs'), $filename);
                }
            }
            $adminRole      = Role::where('name', $request->role)->first();
            $user           = new User;
            $user->fname    = $request->fname;
            $user->lname    = $request->lname;
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            $user->phone    = $request->phone;
            $user->type     = $request->type;
            $user->level_id = $request->level_id;
            $user->img      = $request->img;
            $user->save();
            $user->roles()->attach($adminRole);

            return response()->json(["status"=>"ok","message"=>"Registered Successfully"]);
        } catch (Exception $e) {
            return response()->json(["status"=>"error","message"=>$e]);
        }
        
    }

    public function getUser(Request $request){
        try {
            // return $request->user()->roles();
            $user = User::with('roles')->where('id', $request->user()->id)->first();
            return response()->json(['status'=>'ok','user'=>$user]);
        } catch (Exception $e) {
            return response()->json(["status"=>"error","message"=>$e]);
        }
    }
}
