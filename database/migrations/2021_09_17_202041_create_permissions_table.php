<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->id();
            $table->boolean('user_module')->default(0);
            $table->boolean('add_user')->default(0);
            $table->boolean('edit_user')->default(0);
            $table->boolean('delete_user')->default(0);
            $table->boolean('assign_permission_module')->default(0);
            $table->boolean('add_role')->default(0);
            $table->boolean('custom_ivr_module')->default(0);
            $table->boolean('ringing_ivr_module')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
