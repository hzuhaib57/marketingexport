<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubCategoryIdSubSubCategoryIdLevelIdThumbnailToVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->string('thumbnail')->after('video');
            $table->foreignId('subcategory_id')->constrained('subcategories')->onDelete('cascade')->after('category_id');
            $table->foreignId('subsubcategory_id')->constrained('subsubcategories')->onDelete('cascade')->after('subcategory_id');
            $table->foreignId('level_id')->constrained('levels')->onDelete('cascade')->after('subsubcategory_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            //
        });
    }
}
