<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableVideosChangeDescriptionAndThumbnailAndSubcategoryIdAndSubsubcategoryId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->longText('description')->nullable()->change();
            $table->string('thumbnail')->nullable()->change();
            $table->dropForeign(['subcategory_id','subsubcategory_id']);
            $table->integer('subcategory_id')->nullable()->unsigned()->change();
            $table->integer('subsubcategory_id')->nullable()->unsigned()->change();
            //Remove the following line if disable foreign key
            $table->foreign('subcategory_id')->references('id')->on('subcategories');
            $table->foreign('subsubcategory_id')->references('id')->on('subsubcategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
